package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Administrativo;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Cidade;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Endereco;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Estado;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Funcionario;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.PessoaJuridica;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.RamoAtividade;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Situacao;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Vendedor;

public class Consultas {

	public static void main(String[] args) throws ParseException {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("exerciciojpa01");

		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		massaDeDados(em);

		em.getTransaction().commit();

		cidadeQuantidadeVendedor(em);
		clientePorVendedor(em, "Ezequias");
		ramosAtividade(em, "Calcado");
		funcionariosAtivos(em, Situacao.ATIVO);
		estadosSemCidades(em);
		vendedoresCidade(em, "Feira de Santana");

		em.close();
		emf.close();

	}

	private static void estadosSemCidades(EntityManager em) {
		
		TypedQuery<Estado> estadoSemCidade = em.createQuery("select e from Estado e where size (e.cidades) = 0",
				Estado.class);
		List<Estado> estadosSemCidade = estadoSemCidade.getResultList();

		
		for (Estado estado : estadosSemCidade) {
			System.out.println(estado.getSigla() + " " + estado.getNome());
		}
	}

	private static void vendedoresCidade(EntityManager em, String nomeCidade) {
		
		TypedQuery<Vendedor> vendedoresCidade = em
				.createQuery("select v from Vendedor v where v.endereco.cidade.nome = :cidade", Vendedor.class);
		vendedoresCidade.setParameter("cidade", nomeCidade);
		List<Vendedor> funcResult = vendedoresCidade.getResultList();

		for (Funcionario funcionario : funcResult) {
			System.out.println(funcionario.getNome() + " | " + funcionario.getTelefone());
		}
	}

	private static void funcionariosAtivos(EntityManager em, Situacao situacao) {
		
		TypedQuery<Funcionario> funcionariosAtivos = em
				.createQuery("select f from Funcionario f where f.situacao = :situacao", Funcionario.class);
		funcionariosAtivos.setParameter("situacao", situacao);
		List<Funcionario> listAtivos = funcionariosAtivos.getResultList();

		for (Funcionario funcAtivos : listAtivos) {
			System.out.println(funcAtivos.getCpf() + " | " + funcAtivos.getNome());
		}
	}

	private static void ramosAtividade(EntityManager em, String nomeRamoAtividade) {
		
		Query query = em.createNativeQuery("select " + "f.nome, " + "f.telefone, " + "f.bairro, " + "f.logradouro, "
				+ "f.cidade_sigla " + "from vendedor v " + "inner join  tab_funcionario f on v.cpf = f.cpf "
				+ "inner join vendedor_cliente vc on vc.vendedor = f.cpf "
				+ "inner join pessoajuridica_ramosatividade pjr on pjr.cnpj = vc.cliente "
				+ "inner join ramoatividade r on pjr.ramo_id = r.id " + "where r.nome = :nomeRamoAtividade");
		query.setParameter("nomeRamoAtividade", nomeRamoAtividade);
		List<Object[]> ramosAtividade = query.getResultList();
		for (Object[] objects : ramosAtividade) {
			System.out.println(
					objects[0] + " | " + objects[1] + " | " + objects[2] + " | " + objects[3] + " | " + objects[4]);
		}
	}

	private static void clientePorVendedor(EntityManager em, String nomeVendedor) {
		
		Query clientePorVendedor = em
				.createNativeQuery("select cliente.nome,cliente.bairro, cliente.logradouro, cliente.cidade_sigla"
						+ " from pessoajuridica cliente "
						+ "inner join vendedor_cliente vc on vc.cliente = cliente.cnpj"
						+ " inner join vendedor v on v.cpf = vc.vendedor "
						+ "inner join tab_funcionario f on v.cpf = f.cpf where f.nome = :nomeVendedor");
		clientePorVendedor.setParameter("nomeVendedor", nomeVendedor);
		List<Object[]> clientePorVendedorResult = clientePorVendedor.getResultList();
		for (Object[] objects : clientePorVendedorResult) {
			System.out.println(objects[0] + " | " + objects[1] + " | " + objects[2] + " | " + objects[3]);
		}
	}

	private static void cidadeQuantidadeVendedor(EntityManager em) {
		

		Query qtdPessoaJuridica = em
				.createNativeQuery("select cidade.nome, count(pessoajuridica.nome) as num_pj from  cidade cidade"
						+ " inner join pessoajuridica on pessoajuridica.cidade_sigla = cidade.sigla group by cidade.nome");
		List<Object[]> pessoasJuridicasPorCidades = qtdPessoaJuridica.getResultList();
		for (Object[] objects : pessoasJuridicasPorCidades) {
			System.out.println(objects[0] + " | " + objects[1]);
			}
		}
		private static void massaDeDados(EntityManager em) throws ParseException {
			Estado bahia = new Estado();
			bahia.setSigla("BA");
			bahia.setNome("Bahia");
			em.persist(bahia);

			Estado minas = new Estado();
			minas.setSigla("MG");
			minas.setNome("Minas Gerais");
			em.persist(minas);

			Estado saopaulo = new Estado();
			saopaulo.setSigla("SP");
			saopaulo.setNome("Sao Paulo");
			em.persist(saopaulo);

			Cidade saopaulocidade = new Cidade();
			saopaulocidade.setNome("Sao Paulo");
			saopaulocidade.setSigla("SP");
			saopaulocidade.setEstado(saopaulo);
			em.persist(saopaulocidade);

			Cidade campinas = new Cidade();
			campinas.setNome("Campinas");
			campinas.setSigla("CP");
			campinas.setEstado(saopaulo);
			em.persist(campinas);

			Cidade salvador = new Cidade();
			salvador.setNome("Salvador");
			salvador.setSigla("SSA");
			salvador.setEstado(bahia);
			em.persist(salvador);

			Cidade porto = new Cidade();
			porto.setNome("Porto seguro");
			porto.setSigla("PS");
			porto.setEstado(bahia);
			em.persist(porto);

			Cidade camacari = new Cidade();
			camacari.setNome("Camacari");
			camacari.setSigla("CM");
			camacari.setEstado(bahia);
			em.persist(camacari);
			
			List<Cidade> cidadesbahia = new ArrayList<Cidade>();
			cidadesbahia.add(salvador);
			cidadesbahia.add(camacari);
			cidadesbahia.add(porto);
			bahia.setCidades(cidadesbahia);

			List<Cidade> cidadesaopaulo = new ArrayList<Cidade>();
			cidadesaopaulo.add(saopaulocidade);
			cidadesaopaulo.add(campinas);
			saopaulo.setCidades(cidadesaopaulo);

			Endereco endereco = new Endereco();
			endereco.setBairro("Itapua");
			endereco.setLogradouro("Alameda nossa senhora de fatima");
			endereco.setCidade(salvador);
			
			RamoAtividade transporte = new RamoAtividade();
			transporte.setNome("Transporte");
			em.persist(transporte);

			RamoAtividade alimentos = new RamoAtividade();
			alimentos.setNome("Alimentos");
			em.persist(alimentos);

			RamoAtividade vestuario = new RamoAtividade();
			vestuario.setNome("Vestuario");
			em.persist(vestuario);
			
			PessoaJuridica amanda = new PessoaJuridica();
			amanda.setCnpj("111.111.111-111");
			amanda.setNome("Amanda");
			amanda.setEndereco(endereco);
			amanda.setFaturamento(new BigDecimal(30.6));
			em.persist(amanda);

			PessoaJuridica isabel = new PessoaJuridica();
			isabel.setCnpj("222.222.222-222");
			isabel.setNome("Isabel");
			isabel.setEndereco(endereco);
			isabel.setFaturamento(new BigDecimal(25.0));
			em.persist(isabel);

			PessoaJuridica leandro = new PessoaJuridica();
			leandro.setCnpj("333.333.333-333");
			leandro.setNome("Leandro");
			leandro.setEndereco(endereco);
			leandro.setFaturamento(new BigDecimal(44.0));
			em.persist(leandro);

			PessoaJuridica maria = new PessoaJuridica();
			maria.setCnpj("444.444.444-444");
			maria.setNome("Maria");
			maria.setEndereco(endereco);
			maria.setFaturamento(new BigDecimal(95.0));
			em.persist(maria);

			PessoaJuridica layla = new PessoaJuridica();
			layla.setCnpj("555.555.555-555");
			layla.setNome("Layla");
			layla.setEndereco(endereco);
			layla.setFaturamento(new BigDecimal(145.0));
			em.persist(layla);

			List<RamoAtividade> ramoTransport = new ArrayList<RamoAtividade>();
			ramoTransport.add(transporte);
			isabel.setRamosAtividade(ramoTransport);
			amanda.setRamosAtividade(ramoTransport);

			List<RamoAtividade> ramovestuario = new ArrayList<RamoAtividade>();
			ramovestuario.add(vestuario);
			leandro.setRamosAtividade(ramovestuario);
			maria.setRamosAtividade(ramovestuario);

			SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");



			Vendedor filipe = new Vendedor();
			filipe.setCpf("121.212.121");
			filipe.setDataNascimento(data.parse("22/07/1992"));
			filipe.setEndereco(endereco);
			filipe.setNome("Filipe");
			filipe.setTelefone("14567");
			filipe.setPercentualComissao(new BigDecimal(5.0));
			filipe.setSituacao(Situacao.ATIVO);
			em.persist(filipe);

			Vendedor gabriela = new Vendedor();
			gabriela.setCpf("121.212.126");
			gabriela.setDataNascimento(data.parse("22/08/1990"));
			gabriela.setEndereco(endereco);
			gabriela.setNome("Gabriela");
			gabriela.setTelefone("54288");
			gabriela.setPercentualComissao(new BigDecimal(10.0));
			gabriela.setSituacao(Situacao.ATIVO);
			em.persist(gabriela);

			Vendedor ezequias = new Vendedor();
			ezequias.setCpf("121.212.127");
			ezequias.setDataNascimento(data.parse("01/06/1995"));
			ezequias.setEndereco(endereco);
			ezequias.setNome("Ezequias");
			ezequias.setTelefone("64133");
			ezequias.setPercentualComissao(new BigDecimal(15.0));
			ezequias.setSituacao(Situacao.SUSPENSO);
			em.persist(ezequias);

			Vendedor mariana = new Vendedor();
			mariana.setCpf("121.212.129");
			mariana.setDataNascimento(data.parse("23/12/1994"));
			mariana.setEndereco(endereco);
			mariana.setNome("Mariana");
			mariana.setTelefone("42155");
			mariana.setPercentualComissao(new BigDecimal(20.0));
			mariana.setSituacao(Situacao.SUSPENSO);
			em.persist(mariana);

			Vendedor marilene = new Vendedor();
			marilene.setCpf("221.212.129");
			marilene.setDataNascimento(data.parse("15/02/1997"));
			marilene.setEndereco(endereco);
			marilene.setNome("Marilene");
			marilene.setTelefone("84944");
			marilene.setPercentualComissao(new BigDecimal(25.0));
			marilene.setSituacao(Situacao.ATIVO);
			em.persist(marilene);

			List<PessoaJuridica> clientes = new ArrayList<>();
			clientes.add(leandro);
			clientes.add(isabel);
			filipe.setClientes(clientes);
			gabriela.setClientes(clientes);

			List<Vendedor> vendedores = new ArrayList<>();
			vendedores.add(ezequias);
			vendedores.add(gabriela);
			maria.setVendedores(vendedores);
			layla.setVendedores(vendedores);

			Administrativo fernando = new Administrativo();
			fernando.setCpf("1527");
			fernando.setDataNascimento(data.parse("05/04/1995"));
			fernando.setEndereco(endereco);
			fernando.setNome("Fernando");
			fernando.setTelefone("467");
			fernando.setTurno(11);
			em.persist(fernando);

			Administrativo rogerio = new Administrativo();
			rogerio.setCpf("7459");
			rogerio.setDataNascimento(data.parse("30/09/1981"));
			rogerio.setEndereco(endereco);
			rogerio.setNome("Rogerio");
			rogerio.setTelefone("899");
			rogerio.setTurno(9);
			em.persist(rogerio);

			Administrativo juan = new Administrativo();
			juan.setCpf("6239");
			juan.setDataNascimento(data.parse("20/08/1988"));
			juan.setEndereco(endereco);
			juan.setNome("Juan");
			juan.setTelefone("467");
			juan.setTurno(11);
			em.persist(juan);

		
		}

	}
