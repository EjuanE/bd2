package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotEmpty;

@Entity
@SequenceGenerator(name = "sequence_ramo_atividade", sequenceName = "sq_ramoatividade1", initialValue = 78, allocationSize = 1)
public class RamoAtividade {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_ramo_atividade")
	private Long id;

	@Column(nullable = false, length = 40)
	@NotEmpty
	private String nome;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

	
}
