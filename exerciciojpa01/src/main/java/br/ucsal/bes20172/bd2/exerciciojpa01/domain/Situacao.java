package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

public enum Situacao {
	ATIVO, SUSPENSO;
}
