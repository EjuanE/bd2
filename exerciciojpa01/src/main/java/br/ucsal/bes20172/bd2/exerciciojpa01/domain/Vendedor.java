package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

//import org.hibernate.annotations.ColumnDefault;

import br.ucsal.bes20172.bd2.exerciciojpa01.converters.SituacaoConverter;

@Entity
// @DiscriminatorValue("V")
public class Vendedor extends Funcionario {

	@Column(name = "percentual_comissao", nullable = false, columnDefinition = " numeric(10,2) check (percentual_comissao >=0) ")
	private BigDecimal percentualComissao;

	@Convert(converter = SituacaoConverter.class)
	@Column(nullable = false, columnDefinition = " char(1) default 'A' ")
	// @ColumnDefault(value = "'A'") // N�o � interessante utilizar, pois amarra
	// o aplicativo ao hibernate.
	private Situacao situacao;

	@ManyToMany(mappedBy = "vendedores")
	private List<PessoaJuridica> clientes;

	public BigDecimal getPercentualComissao() {
		return percentualComissao;
	}

	public void setPercentualComissao(BigDecimal percentualComissao) {
		this.percentualComissao = percentualComissao;
	}

	public Situacao getSituacao() {
		return situacao;
	}

	public void setSituacao(Situacao situacao) {
		this.situacao = situacao;
	}

	public List<PessoaJuridica> getClientes() {
		return clientes;
	}

	public void setClientes(List<PessoaJuridica> clientes) {
		this.clientes = clientes;
	}
	
	

}
